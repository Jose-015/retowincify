
==================================================================
General:
API construida con Nodejs 12.19.0, paquete de ArchLinux nodejs-lts-erbium 12.19.0-1, y con mariadb  Ver 15.1 Distrib 10.5.6-MariaDB, for Linux (x86_64) using readline 5.1

para ejecutarlo usar configurar:
"scripts": {
    "test": "echo \"Error: no test specified\" && exit 1",
    "start": "nodemon serverfile/server.js"
},
en package.json y ejecutar con npm start.

para dudas sobre el codigo comunicarse al correo: pumas_bernabe@hotmail.com.

==================================================================
==================================================================
Base de Datos y tabla usada:

La base de Datos tiene por nombre "agenda"
La tabla se llama "user" con elementos:
	id: tipo = Varchar, tamaño = 30, default= null y auto incrementar
	name: tipo = Varchar, tamaño = 30, default= null
	phone: tipo = Varchar, tamaño = 10, default= null 
	email: tipo = Varchar, tamaño = 30, default= null

Se usa el paquete node-mysql-admin para poder administrar la base de datos con una interfaz desde el navegador que se accede con la ruta /myadmin

==================================================================
==================================================================
payload:

Para los roles y grupos permitidos, unicamente se verifica los roles
"roles": [
	"administrador"
]
}

==================================================================
==================================================================

La aplicacion se probo con postman, usando:
Headers:
	Content-Type: application/json
	Authorization: "TOKEN GENERADO"

Body:
	GraphQL: <----- obcion del Body

==================================================================	
La sintaxis sugerida de uso para la funcion create_user es:
Mutation consulte:
	mutation CreateUser($datos: UserInput){
	  create_user( user: $datos){
	    name
	    phone
	    email
	  }
	}
Query Variables:
	{
	 "datos": {
	  "name": "nombre",
	  "phone": "numero",
	  "email": "correo@corre.com"
	 }
	}

==================================================================
La sintaxis sugerida de uso para la funcion edit_user es: 
Mutation Consulte:
	mutation UpdateUser($id: String!, $user: UserInput){
	  edit_user( id: $id,  user: $user){
	    name
	    phone
	    email
	  }
	}
Query Variables:
	{
	 "id": "id",
	 "user": {
	  "name": "nombre",
	  "phone": "nuemero",
	  "email": "cooreo@correo.com"
	 }
	}

==================================================================
La sintaxis sugerida de uso para la funcion delete_user es:
Mutation Consulte:
	mutation DeleteUser($id: String!){
	  delete_user( id: $id)
	}
query Variables:
    {
      "id": "1"
	}

==================================================================
La sintaxis sugerida de uso para la funcion get_user es:
Query Consulte:
	query getUser($id: String!){
	  get_user( id: $id){
	    name
	    phone
	    email
	  }
	}
Query Variables:
	{
	"id": "1"
	}
