const jwt = require('jsonwebtoken');
const { AuthenticationError, } = require('apollo-server-express');

const secret = "dummiest secret"; //Secreto del JWT

const verificarToken = function(token){
	try {
		const resul = jwt.verify(token, secret,{algorithm: 'HS256'});
		return (resul.roles);
    } catch (e) {
        throw new AuthenticationError(
            'Authentication token is invalid, please log in',
        );
    }

}

module.exports = {
	secret: secret,
	verificarToken: verificarToken
}