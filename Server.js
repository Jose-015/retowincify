/*
* Nota personal: Nodejs 12.19.0; paquete de ArchLinux nodejs-lts-erbium 12.19.0-1
* Importacion de Librerias
*/

const morgan = require('morgan');
//const mysql = require('mysql');
const mysqlAdmin = require('node-mysql-admin'); //Administrador grafico para la based de datos se accede con /myadmin
const { graphqlHTTP } = require('express-graphql');
const express = require('express');
const app = express();


app.set('port', process.env.PORT || 3000);
app.use(morgan('dev'));
app.use(mysqlAdmin(app)); 

/* =========================================================================================00
* Configuracion de conexion al servidor
*/
const connection = require('./configuracionMysql');
connection.connect(function(error){
   if(error){
      throw error;
      console.log('Error conexion incorrecta.');
   }else{
      console.log('Conexion correcta.');
   }
});

/* =========================================================================================00
* Configuraciones de GraphQL
* Declaracion del esquema para GraphQL
* schema: es el esquema del tipo de datos
* createUser: funcion para crear usuarios
* editUser: funcion para editar usuarior
* deleteUser: funcion para borrar usuario
* getUser: funcion para obtener usuario
*/
const {schema, createUser, editUser, deleteUser, getUser } = require('./configuracionGraphQL');

/*
* Declaracion de funciones de resolucion de GraphQL
*/
const root = {
    create_user: createUser,
    edit_user: editUser,
    delete_user: deleteUser,
    get_user: getUser,
   	Test(args,context){ return "hola "+args.name} //Para testeo
};

/* =========================================================================================00
* Rutas de acceso
* Ruta Principal de GraphQL usando metodo Post
*/
app.post('/graphql', graphqlHTTP({
    schema: schema,
    rootValue: root,
    //graphiql: false //Al activarlo se usa app.use
}));//*/


/*
* Inicio del servidor
*/
app.listen(app.get('port'), ()=>{
	console.log(`Servidor iniciado ${app.get('port')}`);
});

//connection.end();