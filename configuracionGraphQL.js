const { graphql, buildSchema } = require('graphql');
const connection = require('./configuracionMysql');
const {secret, verificarToken} = require('./configuracionJWT');

const schema = buildSchema(
    `
    type Query {
    	get_user( id : String! ) : User
    	Test( name: String!): String
    }

    type Mutation {
    	create_user( user : UserInput ) : User
    	edit_user( id : String! , user : UserInput ) : User
    	delete_user( id : String! ) : String
    }
    type User {
    	name : String!
    	phone : String!
    	email : String!
    }
    input UserInput {
    	name : String!
    	phone : String!
    	email : String
    }
`);

/*
* Funciones
*/
const createUser = async function(args, context){
    console.log("Argumentos entrantes: ",args);
    var token = context.headers.authorization;
    token = verificarToken(token);

    var name = args.user.name;
    var phone = args.user.phone;
    var email = args.user.email; //Pendiente caso de Null ya que puedo ser omitido por el cliente
    
    return new Promise( (resolve, reject) =>{
        if(token != 'administrador'){
            reject("No tienes permisos de administrador");
        }

        connection.query('SELECT * FROM user where name=? OR phone=? OR email=? ',[name, phone, email], function (err, result, fields) {
            if (err){
                reject(err);
            }else{
                console.log("Resultado de la busqueda: ",result);
                if(result.length === 0){
                    console.log("No existen los datos a registrar");
                    var query = connection.query('INSERT INTO user(name, phone, email) VALUES(?,?,?)',[name, phone, email], function(error, result){
                        if(error){
                            reject(error);
                        }else{
                            console.log("Dato Registrado");
                            resolve(args.user);
                        }
                    });
                }else{
                    if(result[0].name == name){
                        console.log("El nombre de usuario existe");
                        reject("El nombre de usuario existe");
                    }
                    if(result[0].phone == phone){
                        console.log("El tefono se encuentra registrado");
                        reject("El telefono se encuentra registrado");
                    }
                    if(result[0].email == email){
                        console.log("El correo se encuentra registrado");
                        reject("El correo se encuentra registrado");
                    }
                }
            }
        });
    });
}
/*  */
const editUser = function (args, context){
    console.log("Argumentos entrantes: ",args);

    var token = context.headers.authorization;
    token = verificarToken(token);

    var id = parseInt(args.id);
    var name = args.user.name;
    var phone = args.user.phone;
    var email = args.user.email; //Pendiente caso de Null ya que puedo ser omitido por el cliente
    
    return new Promise( (resolve, reject) =>{
        if(token != 'administrador'){
            reject("No tienes permisos de administrador");
        }
        console.log("Acceso Permitido");
        connection.query('UPDATE user SET name=?, phone=?, email=? WHERE id=?',[name,phone, email,id], function (err, result, fields) {
            if (err){
                reject(err);
            }else{
                console.log(result);
                if(result.affectedRows== 0){ reject("el "+id+" ya no existe")} // affectedRowsaffectedRowsVERICICA
                resolve(args.user);
            }
        });
    });
}
/* */
const deleteUser = function(args, context){
    console.log("Argumentos entrantes: ",args);
    var token = context.headers.authorization;
    token = verificarToken(token);
    
    var id = parseInt(args.id);
    return new Promise( (resolve, reject) =>{
        if(token != 'administrador'){
            reject("No tienes permisos de administrador");
        }
        console.log("Acceso Permitido");

        connection.query('DELETE FROM user WHERE id=?',[id], function (err, result, fields) {
            if (err){
                reject(err);
            }else{
                console.log("Resultado de la busqueda: ",result);
                resolve("Elemento Borrado");
            }
        });
    });
}
/* */
const getUser = function (args, context){
    console.log("Argumentos entrantes: ",args);
    var token = context.headers.authorization;
    
    token = verificarToken(token);
    console.log(token);

    const id = parseInt(args.id);
    return new Promise( (resolve, reject) =>{
        if(token != 'administrador'){
            reject("No tienes permisos de administrador");
        }
        console.log("Acceso Permitido");
        
        connection.query('SELECT * FROM user where id=? ',[id], function (err, result, fields) {
            if (err){
                reject(err);
            }else{
                if(result.length == 0){ 
                    console.log("no existe el elemento"); 
                    resolve({
                        name:'no existe', phone:'no existe', email:'no existe'
                    });
                }else{
                    console.log("El usuario existe: ",result[0]);
                    resolve({
                        name: result[0].name, phone:result[0].phone, email:result[0].email
                    });
                }
            }
        });
    });
}


module.exports = {
    schema: schema,
    createUser: createUser,
    editUser: editUser,
    deleteUser: deleteUser,
    getUser: getUser
}