FROM node:12-alpine

WORKDIR /app


COPY configuracionGraphQL.js /app
COPY configuracionJWT.js /app 
COPY configuracionMysql.js /app 
COPY package.json /app
COPY Server.js /app

RUN npm install
RUN npm install nodemon

EXPOSE 3000

ENTRYPOINT npm start

